import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="wekan-generator",
    version="0.1.0",
    author="Sanchan Moses",
    author_email="sanchanm@wekan.company",
    description="Project generator for wekan's python framework",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/We-Kan-Code/python-backend-framework-generators",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    entry_points={
        'console_scripts': [
            'wekan = src.generate:main',
        ],
    },
)
