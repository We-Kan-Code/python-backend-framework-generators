# Backend Framework - Generator

Wekan's [Backend Framework](https://bitbucket.org/We-Kan-Code/python-backend-framework/) project and model generators

The CLI tool helps you initialize and develop applications. It assists in scaffolding the project and generating new modules.

The generated project is built on top of flask-restful, sqlalchemy and provides authentication, access control (RBAC) endpoints out of the box. Generated modules have built-in endpoints for basic CRUD operations.

## Getting started

To install just run

`pip install wekan -U`

and then `wekan` to get started

_Keep the model names singular, the generator converts the name to its plural form where requried._
